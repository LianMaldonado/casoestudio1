<footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navegaciones</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Venta online</a></li>
                  <li><a href="#">Funciones</a></li>
                  <li><a href="#">Vender productos</a></li>
                  <li><a href="#">Constructor de tiendas</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Comercio movil</a></li>
                  <li><a href="#">Carrito</a></li>
                  <li><a href="#">Desarrollo de la Web</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="#">Punto de venta</a></li>
                  <li><a href="#">Hardware</a></li>
                  <li><a href="#">Software</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Promo</h3>
            <a href="#" class="block-6">
              <img src="images/libro15.jpg" height="150" width="120" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Encontrando tu libro perfecto</h3>
              <p>Promo desde Marzo 14 &mdash; 25, 2021</p>
            </a>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Informacion de contacto</h3>
              <ul class="list-unstyled">
                <li class="address"> AV. Vallarta 8080 Colonia Mirador, Guadalajara, Jalisco, Mexico </li>
                <li class="phone"><a href="tel://3313486301">+52 33-13-48-63-01</a></li>
                <li class="email">email@gmail.com</li>
              </ul>
            </div>

            <div class="block-7">
              <form action="#" method="post">
                <label for="email_subscribe" class="footer-heading">Inscribete</label>
                <div class="form-group">
                  <input type="text" class="form-control py-4" id="email_subscribe" placeholder="Email">
                  <input type="submit" class="btn btn-sm btn-primary" value="Send">
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
         
            Copyright &copy;
            <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
            <script>document.write(new Date().getFullYear());</script> All rights reserved | 
            Tienda de Libros <i class="icon-heart" aria-hidden="true"></i>
        
            </p>
          </div>
          
        </div>
      </div>
    </footer>